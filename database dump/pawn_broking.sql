-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 05, 2015 at 08:29 PM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pawn_broking`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `article_name` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `article_name` (`article_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `article_name`) VALUES
(1, 'Chain'),
(2, 'Ring'),
(3, 'Bangle'),
(4, 'Necklace'),
(5, 'Earrings'),
(6, 'Talisman'),
(7, 'Gold Coins'),
(8, 'Pendent'),
(9, 'Bracelet'),
(10, 'Gold Biscuit');

-- --------------------------------------------------------

--
-- Table structure for table `auctions`
--

CREATE TABLE IF NOT EXISTS `auctions` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `ticket_no` varchar(10) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `auction_status` char(1) NOT NULL,
  `staff` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ticket_no` (`ticket_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `auctions`
--

INSERT INTO `auctions` (`id`, `ticket_no`, `date`, `auction_status`, `staff`) VALUES
(1, 'T000015', '2015-03-15 08:00:33', 'u', 4),
(2, 'T000010', '2015-03-16 07:08:32', 'u', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cancelled_ticket_articles`
--

CREATE TABLE IF NOT EXISTS `cancelled_ticket_articles` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `ticket_no` varchar(15) NOT NULL,
  `article` varchar(24) NOT NULL,
  `karat` int(2) NOT NULL,
  `gross_weight` float(4,2) NOT NULL,
  `net_weight` float(4,2) NOT NULL,
  `assessed_val` float(8,2) NOT NULL,
  `payable_val` float(8,2) NOT NULL,
  `cancelled_date` datetime NOT NULL,
  `staff` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ticket_no` (`ticket_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cancelled_ticket_articles`
--

INSERT INTO `cancelled_ticket_articles` (`id`, `ticket_no`, `article`, `karat`, `gross_weight`, `net_weight`, `assessed_val`, `payable_val`, `cancelled_date`, `staff`) VALUES
(1, 'T15022336', 'Chain', 16, 4.00, 4.00, 16500.00, 14850.00, '2015-02-23 21:46:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `nic` varchar(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `city` varchar(64) NOT NULL,
  `postal_code` varchar(6) NOT NULL,
  `email` varchar(64) NOT NULL,
  `contact_no` int(10) NOT NULL,
  `mobile_no` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nic` (`nic`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `nic`, `name`, `address1`, `address2`, `city`, `postal_code`, `email`, `contact_no`, `mobile_no`) VALUES
(1, '861130055v', 'nalaka', '50, church road,', 'Kalutara south', 'kalutara', '11', 'nalakanayana@gmail.com', 2147483647, 2147483647),
(2, '451130055v', 'A.K Kodikaara', '21/2, samagi mawatha,', 'Atulugama,', 'kalutara', '12000', 'mail2farzath@yahoo.com', 2147483647, 1258623902),
(3, '785130055v', 'KP Poll', '12, govinda road,', 'Kalutara south', 'kalutara', '11', 'kp@yahoo.com', 1845116518, 771299595),
(8, '651130055v', 'nalakan', '78, galle road,', 'panadura', 'panadura', '', 'pk@yahoo.com', 2147483647, 772605427),
(10, '785123455v', 'Gamage', 'Nimal road', 'bambalapitiya', 'colombo', '', 'game@gamage.com', 1234567892, 2147483647),
(16, '833660500v', 'nalaka', 'No. 39u, Alvis place,', 'Kollupitiya,', 'Colombo', '', 'nalakanayana@gmail.com', 2147483647, 2147483647),
(17, '782226533v', 'Appuhaami', '21/2, samagi mawatha,', 'Kalutaa north', 'kalutara', '', '', 1154545121, 0),
(18, '632326565v', 'Palitha', 'Galle road,', 'Kalutara south', 'kalutara', '', 'palitha@yahoo.com', 2147483647, 0),
(19, '821655913v', 'Peter hain', 'Agalawatta', 'matugama', 'kalutara', '', 'peter@gmail.com', 2147483647, 771299595),
(20, '861365959x', 'Tissa', '# 78, Silva mawatha', 'Colombo 7', 'Colombo', '', 'farza86@yahoo.com', 2147483647, 2147483647),
(21, '879669962v', 'Amal priyankara', '#49,  araliya uyana', 'panadura', 'kalutara', '12000', 'farzath@mooiwebs.com', 2147483647, 2147483647),
(22, '8336605000', 'naaalaka', '', '', '', '', '', 772605427, 772605427),
(23, '8510102020', 'KP Poll', '50, church road,', 'gg', 'dgdg', '', 'erererrer@yahoo.com', 1845116518, 2147483647);

-- --------------------------------------------------------

--
-- Table structure for table `inquiries`
--

CREATE TABLE IF NOT EXISTS `inquiries` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(10) NOT NULL,
  `customer_inquiry` text NOT NULL,
  `inquiry_date` datetime NOT NULL,
  `mgr_reply` text,
  `replied_date` datetime DEFAULT NULL,
  `mgr` char(1) DEFAULT NULL,
  `attachment` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `inquiries`
--

INSERT INTO `inquiries` (`id`, `customer_id`, `customer_inquiry`, `inquiry_date`, `mgr_reply`, `replied_date`, `mgr`, `attachment`) VALUES
(1, '861130055v', 'This is my test inquiry form submissions', '2015-02-22 07:29:47', 'OK', '2015-02-25 16:14:49', '4', '/customer/attachments/10658580_768335553216227_9099777551123061548_o.jpg'),
(2, '451130055v', 'I have an inquiry about my pawn ticket T00002, what is the interest amout i have to pay?', '2015-02-22 13:48:27', 'OK', '2015-02-25 16:14:45', '4', ''),
(3, '861365959x', 'test tissa', '2015-02-25 09:40:43', 'OK', '2015-02-25 16:14:43', '4', '/customer/attachments/2015-02-19_143648.jpg'),
(4, '833660500v', 'test', '2015-03-14 07:32:12', 'ok see', '2015-03-14 07:34:39', '4', '/customer/attachments/SLIPS IP.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `karatage`
--

CREATE TABLE IF NOT EXISTS `karatage` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `karat_value` int(2) NOT NULL,
  `assessed_value` float(8,2) NOT NULL,
  `payable_value` float(8,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `karat_value` (`karat_value`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `karatage`
--

INSERT INTO `karatage` (`id`, `karat_value`, `assessed_value`, `payable_value`) VALUES
(1, 24, 30555.00, 27500.00),
(2, 23, 27779.00, 25001.00),
(3, 22, 27779.00, 25001.00),
(4, 21, 26555.00, 23900.00),
(5, 20, 25222.00, 22700.00),
(6, 19, 24000.00, 21600.00),
(7, 18, 22777.00, 20499.00),
(8, 17, 21444.00, 19300.00),
(9, 16, 20222.00, 18200.00),
(10, 14, 17666.00, 15899.00);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `time_zone` varchar(20) NOT NULL,
  `interest_rate` double(20,2) NOT NULL,
  `session_time_out` int(20) NOT NULL,
  `min_net_weight` double(20,0) DEFAULT NULL,
  `min_advance_amount` double(20,0) DEFAULT NULL,
  `min_payment` double(20,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`time_zone`, `interest_rate`, `session_time_out`, `min_net_weight`, `min_advance_amount`, `min_payment`) VALUES
('Asia/Colombo', 12.00, 1800, 4, 1200, 200);

-- --------------------------------------------------------

--
-- Table structure for table `payment_history`
--

CREATE TABLE IF NOT EXISTS `payment_history` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `ticket_no` varchar(15) NOT NULL,
  `pay_date` datetime NOT NULL,
  `pay_amount` float(8,2) NOT NULL,
  `paid_capital` float(8,2) NOT NULL,
  `paid_interest` float(8,2) NOT NULL,
  `staff` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ticket_no_2` (`ticket_no`,`pay_date`),
  KEY `ticket_no` (`ticket_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `payment_history`
--

INSERT INTO `payment_history` (`id`, `ticket_no`, `pay_date`, `pay_amount`, `paid_capital`, `paid_interest`, `staff`) VALUES
(1, 'T000001', '2015-01-01 23:51:28', 1000.00, 0.00, 1000.00, 0),
(2, 'T000002', '2014-01-01 23:51:28', 5000.00, 2475.04, 2524.96, 0),
(5, 'T000002', '2014-01-07 00:12:43', 38413.20, 37524.96, 888.24, 0),
(6, 'T000009', '2014-04-01 23:51:28', 5000.00, 4842.20, 157.80, 0),
(7, 'T000009', '2014-06-23 10:24:15', 2500.00, 2201.46, 298.54, 1),
(8, 'T000027', '2014-08-28 17:59:25', 10000.00, 5739.16, 4260.84, 1),
(9, 'T000024', '2014-08-02 10:05:14', 10000.00, 7317.23, 2682.77, 1),
(10, 'T000019', '2015-02-02 10:23:26', 5000.00, 3323.29, 1676.71, 1),
(11, 'T000028', '2015-02-12 11:28:29', 50000.00, 27906.88, 22093.12, 1),
(12, 'T000019', '2015-02-14 14:24:50', 25000.00, 0.00, 25000.00, 1),
(13, 'T000019', '2015-02-16 21:52:29', 24043.11, 21676.71, 2366.40, 1),
(14, 'T000024', '2015-02-17 18:30:56', 5000.00, 0.00, 5000.00, 1),
(15, 'T15022543', '2015-02-25 21:45:29', 1000.00, 1000.00, 0.00, 4),
(16, 'T15022645', '2015-02-27 11:19:27', 105523.29, 100000.00, 5523.29, 4),
(17, 'T15022748', '2015-02-27 13:15:05', 21104.66, 20000.00, 1104.66, 6),
(18, 'T15022538', '2015-02-27 20:30:37', 10000.00, 9944.77, 55.23, 4),
(19, 'T15022747', '2015-02-27 20:42:48', 200.00, 107.95, 92.05, 4),
(20, 'T000030', '2015-02-28 13:19:42', 1000.00, 0.00, 1000.00, 4),
(21, 'T15022644', '2015-03-01 23:15:41', 1000.00, 986.19, 13.81, 4),
(22, 'T15031450', '2015-03-14 11:29:31', 1000.00, 999.67, 0.33, 4),
(23, 'T15031450', '2015-03-14 11:37:19', 0.33, 0.33, 0.00, 4),
(24, 'T000024', '2015-03-14 11:55:52', 10000.00, 0.00, 10000.00, 4),
(25, 'T15031451', '2015-03-14 12:46:22', 1500.49, 1500.00, 0.49, 4),
(26, 'T15031449', '2015-03-14 12:52:26', 1000.33, 1000.00, 0.33, 4),
(27, 'T15031456', '2015-03-15 09:50:00', 1500.98, 1500.00, 0.98, 4),
(28, 'T15031558', '2015-03-15 09:51:02', 23007.56, 23000.00, 7.56, 4),
(29, 'T15022747', '2015-03-15 09:59:04', 200.00, 88.82, 111.18, 4),
(30, 'T000029', '2015-03-15 10:03:57', 200.00, 0.00, 200.00, 4),
(31, 'T000028', '2015-03-15 10:09:39', 200.00, 0.00, 200.00, 4),
(32, 'T15031455', '2015-03-15 10:16:09', 1500.98, 1500.00, 0.98, 4),
(33, 'T15022747', '2015-03-15 10:30:08', 19809.74, 19803.23, 6.51, 4),
(34, 'T15031559', '2015-03-15 10:33:57', 1200.39, 1200.00, 0.39, 4),
(35, 'T15031561', '2015-03-15 11:05:32', 2000.66, 2000.00, 0.66, 4),
(36, 'T000010', '2015-03-15 11:11:17', 1000.00, 0.00, 1000.00, 4),
(37, 'T15031563', '2015-03-15 16:16:15', 5001.64, 5000.00, 1.64, 4),
(38, 'T000015', '2015-03-15 16:31:39', 1000.00, 0.00, 1000.00, 4),
(39, 'T15031560', '2015-03-15 16:40:10', 6503.12, 6500.98, 2.14, 4),
(42, 'T15031452', '2015-03-15 20:19:41', 1000.66, 1000.00, 0.66, 4),
(43, 'T000015', '2015-03-15 20:34:33', 200.00, 0.00, 200.00, 4),
(45, 'T000015', '2015-03-16 09:33:28', 200.00, 0.00, 200.00, 4);

-- --------------------------------------------------------

--
-- Table structure for table `repawn_history`
--

CREATE TABLE IF NOT EXISTS `repawn_history` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `old_ticket_no` varchar(15) NOT NULL,
  `new_ticket_no` varchar(15) NOT NULL,
  `customer_id` varchar(10) NOT NULL,
  PRIMARY KEY (`id`,`old_ticket_no`),
  KEY `new_ticket_no` (`new_ticket_no`),
  KEY `new_ticket_no_2` (`new_ticket_no`),
  KEY `customer_id` (`customer_id`),
  KEY `old_ticket_no` (`old_ticket_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `repawn_history`
--

INSERT INTO `repawn_history` (`id`, `old_ticket_no`, `new_ticket_no`, `customer_id`) VALUES
(1, 'T000001', 'T000015', '861130055v'),
(2, 'T000001', 'T000015', '861130055v'),
(3, 'T000009', 'T000029', '785130055v'),
(4, 'T15031450', 'T15031451', '833660500v'),
(5, 'T15031449', 'T15031453', '833660500v'),
(6, 'T15031456', 'T15031558', '8510102020'),
(7, 'T15031558', 'T15031559', '8510102020'),
(8, 'T15031455', 'T15031560', '8336605000'),
(9, 'T15022747', 'T15031561', '833660500v'),
(10, 'T15031559', 'T15031562', '8510102020'),
(11, 'T15031561', 'T15031563', '833660500v'),
(12, 'T15031563', 'T15031566', '833660500v'),
(13, 'T15031560', 'T15031567', '8336605000'),
(16, 'T15031452', 'T15031570', '833660500v');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE IF NOT EXISTS `tickets` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `ticket_no` varchar(15) NOT NULL,
  `customer_id` varchar(12) NOT NULL,
  `tot_payable_val` float(8,2) NOT NULL,
  `advance_amt` float(8,2) NOT NULL,
  `interest_rate` varchar(3) NOT NULL,
  `pawn_officer` int(2) NOT NULL,
  `pawn_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ticket_no` (`ticket_no`),
  UNIQUE KEY `ticket_no_2` (`ticket_no`,`customer_id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `ticket_no`, `customer_id`, `tot_payable_val`, `advance_amt`, `interest_rate`, `pawn_officer`, `pawn_date`) VALUES
(1, 'T000001', '861130055v', 66600.00, 40000.00, '10%', 1, '2013-12-11 23:51:28'),
(8, 'T000002', '451130055v', 66262.00, 40000.00, '12%', 1, '2013-12-21 21:13:27'),
(9, 'T000009', '785130055v', 13500.00, 10000.00, '8%', 1, '2014-01-03 13:53:30'),
(14, 'T000010', '651130055v', 72000.00, 50000.00, '12%', 1, '2014-01-01 19:06:44'),
(18, 'T000015', '861130055v', 74000.00, 30000.00, '12%', 1, '2013-08-14 21:55:38'),
(23, 'T000019', '785123455v', 40000.00, 25000.00, '12%', 1, '2014-03-16 10:50:08'),
(26, 'T000024', '833660500v', 52650.00, 40000.00, '12%', 1, '2014-06-16 11:56:51'),
(27, 'T000027', '782226533v', 98100.00, 90000.00, '12%', 1, '2014-08-16 16:13:49'),
(28, 'T000028', '632326565v', 68006.25, 50000.00, '12%', 1, '2014-10-23 12:02:06'),
(29, 'T000029', '785130055v', 12035.88, 12964.12, '12%', 1, '2014-06-23 18:37:28'),
(30, 'T000030', '821655913v', 110000.00, 100000.00, '12%', 1, '2015-01-28 17:55:29'),
(32, 'T000031', '861130055v', 30000.00, 20000.00, '12%', 1, '2015-02-08 21:59:17'),
(33, 'T000033', '861365959x', 33300.00, 25000.00, '12%', 1, '2015-02-11 22:17:11'),
(34, 'T000034', '879669962v', 59400.00, 4000.00, '12%', 1, '2015-02-14 13:04:59'),
(35, 'T15021835', '833660500v', 51975.00, 40000.00, '12%', 1, '2015-02-18 15:21:22'),
(36, 'T15022336', '861365959x', 40000.00, 35000.00, '12', 1, '2015-02-23 17:13:57'),
(37, 'T15022537', '861130055v', 27500.00, 15000.00, '12', 5, '2015-02-25 18:44:05'),
(38, 'T15022538', '785130055v', 27500.00, 12000.00, '12', 5, '2015-02-25 18:54:36'),
(39, 'T15022539', '861130055v', 13750.00, 1500.00, '12', 5, '2015-02-25 20:37:41'),
(40, 'T15022540', '821655913v', 41975.00, 25000.00, '12', 1, '2015-02-25 20:39:24'),
(41, 'T15022541', '861130055v', 27500.00, 10000.00, '12', 4, '2015-02-25 21:06:59'),
(42, 'T15022542', '785130055v', 37501.50, 1222.00, '12', 4, '2015-02-25 21:20:39'),
(43, 'T15022543', '785130055v', 37501.50, 1222.00, '12', 4, '2015-02-25 21:21:15'),
(44, 'T15022644', '861130055v', 13750.00, 3000.00, '12', 4, '2015-02-26 09:13:24'),
(45, 'T15022645', '785130055v', 115937.50, 100000.00, '12', 4, '2015-02-26 09:15:08'),
(46, 'T15022746', '833660500v', 23900.00, 20000.00, '12', 4, '2015-02-27 11:15:43'),
(47, 'T15022747', '833660500v', 27500.00, 20000.00, '12', 4, '2015-02-27 11:17:45'),
(48, 'T15022748', '861130055v', 27500.00, 20000.00, '12', 6, '2015-02-27 12:54:31'),
(49, 'T15031449', '833660500v', 27500.00, 1000.00, '12', 4, '2015-03-14 11:16:56'),
(50, 'T15031450', '833660500v', 27500.00, 1000.00, '12', 4, '2015-03-14 11:21:13'),
(51, 'T15031451', '833660500v', 27499.67, 1500.00, '12', 4, '2015-03-14 11:37:19'),
(52, 'T15031452', '833660500v', 27500.00, 1000.00, '12', 4, '2015-03-14 12:08:13'),
(53, 'T15031453', '833660500v', 26499.67, 25000.00, '12', 4, '2015-03-14 12:52:26'),
(54, 'T15031454', '833660500v', 27500.00, 1000.00, '12', 5, '2015-03-14 13:37:28'),
(55, 'T15031455', '8336605000', 27500.00, 1500.00, '12', 4, '2015-03-14 21:07:59'),
(56, 'T15031456', '8510102020', 26100.00, 1500.00, '12', 4, '2015-03-14 21:09:05'),
(57, 'T15031457', '8510102020', 25001.00, 1500.00, '12', 4, '2015-03-14 21:12:36'),
(58, 'T15031558', '8510102020', 23500.02, 23000.00, '12', 4, '2015-03-15 09:50:00'),
(59, 'T15031559', '8510102020', 1993.44, 1200.00, '12', 4, '2015-03-15 09:51:02'),
(60, 'T15031560', '8336605000', 25999.02, 6500.98, '12', 4, '2015-03-15 10:16:09'),
(61, 'T15031561', '833660500v', 7690.26, 2000.00, '12', 4, '2015-03-15 10:30:08'),
(62, 'T15031562', '8510102020', 23800.61, 23800.00, '12', 4, '2015-03-15 10:33:57'),
(63, 'T15031563', '833660500v', 25499.34, 5000.00, '12', 4, '2015-03-15 11:05:32'),
(64, 'T15031564', '833660500v', 27500.00, 1200.00, '12', 4, '2015-03-15 16:05:17'),
(65, 'T15031565', '833660500v', 27500.00, 15000.00, '12', 4, '2015-03-15 16:06:02'),
(66, 'T15031566', '833660500v', 22498.36, 22000.00, '12', 4, '2015-03-15 16:16:15'),
(67, 'T15031567', '8336605000', 20996.88, 6504.00, '12', 4, '2015-03-15 16:40:10'),
(68, 'T15031568', '833660500v', 26499.34, 1500.00, '12', 4, '2015-03-15 20:01:57'),
(69, 'T15031569', '833660500v', 26499.34, 2000.00, '12', 4, '2015-03-15 20:02:37'),
(70, 'T15031570', '833660500v', 26499.34, 2500.00, '12', 4, '2015-03-15 20:19:41'),
(71, 'T15031571', '833660500v', 25001.00, 12000.00, '12', 4, '2015-03-15 20:32:34'),
(72, 'T15031572', '833660500v', 25001.00, 10000.00, '12', 4, '2015-03-15 20:33:17'),
(73, 'T15031673', '833660500v', 27500.00, 27500.00, '12', 4, '2015-03-16 09:29:18');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_articles`
--

CREATE TABLE IF NOT EXISTS `ticket_articles` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `ticket_no` varchar(15) NOT NULL,
  `article` varchar(24) NOT NULL,
  `karat` int(2) NOT NULL,
  `gross_weight` float(4,2) NOT NULL,
  `net_weight` float(4,2) NOT NULL,
  `assessed_val` float(8,2) NOT NULL,
  `payable_val` float(8,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ticket_no` (`ticket_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=82 ;

--
-- Dumping data for table `ticket_articles`
--

INSERT INTO `ticket_articles` (`id`, `ticket_no`, `article`, `karat`, `gross_weight`, `net_weight`, `assessed_val`, `payable_val`) VALUES
(1, 'T000001', 'Bangle', 24, 4.00, 3.00, 15000.00, 13500.00),
(2, 'T000001', 'Bangle', 24, 7.52, 6.80, 34000.00, 30600.00),
(3, 'T000001', 'Chain', 24, 5.00, 5.00, 25000.00, 22500.00),
(4, 'T000002', 'Bangle', 24, 7.52, 6.80, 34000.00, 30600.00),
(5, 'T000002', 'Chain', 20, 5.00, 5.00, 23125.00, 20812.50),
(6, 'T000002', 'Pendent', 16, 5.00, 4.00, 16500.00, 14850.00),
(7, 'T000009', 'Bangle', 24, 4.00, 3.00, 15000.00, 13500.00),
(13, 'T000010', 'Gold Biscuit', 24, 16.00, 16.00, 80000.00, 72000.00),
(17, 'T000015', 'Bangle', 24, 4.00, 3.00, 16875.00, 15000.00),
(18, 'T000015', 'Bangle', 24, 7.52, 6.80, 38250.00, 34000.00),
(19, 'T000015', 'Chain', 24, 5.00, 5.00, 28125.00, 25000.00),
(22, 'T000019', 'Bangle', 24, 8.00, 8.00, 45000.00, 40000.00),
(29, 'T000024', 'Gold Coins', 22, 8.00, 8.00, 39000.00, 35100.00),
(30, 'T000024', 'Earrings', 22, 4.00, 4.00, 19500.00, 17550.00),
(31, 'T000027', 'Necklace', 20, 16.00, 16.00, 74000.00, 66600.00),
(32, 'T000027', 'Earrings', 18, 8.00, 8.00, 35000.00, 31500.00),
(33, 'T000028', 'Pendent', 22, 16.00, 15.50, 75562.50, 68006.25),
(34, 'T000029', 'Bangle', 24, 4.00, 3.00, 16875.00, 15000.00),
(35, 'T000030', 'Gold Biscuit', 24, 24.00, 22.00, 123750.00, 110000.00),
(37, 'T000031', 'Gold Biscuit', 24, 6.00, 6.00, 33750.00, 30000.00),
(38, 'T000033', 'Talisman', 20, 8.00, 8.00, 37000.00, 33300.00),
(39, 'T000034', 'Necklace', 16, 16.00, 16.00, 66000.00, 59400.00),
(40, 'T15021835', 'Earrings', 20, 6.00, 6.00, 27750.00, 24975.00),
(41, 'T15021835', 'Ring', 14, 8.00, 8.00, 30000.00, 27000.00),
(42, 'T15022336', 'Bracelet', 24, 8.00, 8.00, 45000.00, 40000.00),
(43, 'T15022537', 'Bangle', 24, 8.00, 8.00, 30555.00, 27500.00),
(44, 'T15022538', 'Bangle', 24, 8.00, 8.00, 30555.00, 27500.00),
(45, 'T15022539', 'Gold Coins', 24, 12.00, 4.00, 15277.50, 13750.00),
(46, 'T15022540', 'Chain', 24, 8.00, 8.00, 30555.00, 27500.00),
(47, 'T15022540', 'Earrings', 17, 6.00, 6.00, 16083.00, 14475.00),
(48, 'T15022541', 'Bracelet', 24, 8.00, 8.00, 30555.00, 27500.00),
(49, 'T15022542', 'Bracelet', 22, 12.00, 12.00, 41668.50, 37501.50),
(50, 'T15022543', 'Bracelet', 22, 12.00, 12.00, 41668.50, 37501.50),
(51, 'T15022644', 'Earrings', 24, 4.00, 4.00, 15277.50, 13750.00),
(52, 'T15022645', 'Earrings', 24, 12.00, 12.00, 45832.50, 41250.00),
(53, 'T15022645', 'Bracelet', 21, 25.00, 25.00, 82984.38, 74687.50),
(54, 'T15022746', 'Bracelet', 21, 8.00, 8.00, 26555.00, 23900.00),
(55, 'T15022747', 'Bracelet', 24, 8.00, 8.00, 30555.00, 27500.00),
(56, 'T15022748', 'Chain', 24, 8.00, 8.00, 30555.00, 27500.00),
(57, 'T15031449', 'Bangle', 24, 8.00, 8.00, 30555.00, 27500.00),
(58, 'T15031450', 'Bracelet', 24, 8.00, 8.00, 30555.00, 27500.00),
(59, 'T15031451', 'Bracelet', 24, 8.00, 8.00, 30555.00, 27500.00),
(60, 'T15031452', 'Bangle', 24, 8.00, 8.00, 30555.00, 27500.00),
(61, 'T15031453', 'Bangle', 24, 8.00, 8.00, 30555.00, 27500.00),
(62, 'T15031454', 'Bracelet', 24, 8.00, 8.00, 30555.00, 27500.00),
(63, 'T15031455', 'Earrings', 24, 8.00, 8.00, 30555.00, 27500.00),
(64, 'T15031456', 'Chain', 22, 8.00, 8.00, 27779.00, 26100.00),
(65, 'T15031457', 'Bracelet', 23, 12.00, 8.00, 27779.00, 25001.00),
(66, 'T15031558', 'Chain', 22, 8.00, 8.00, 27779.00, 25001.00),
(67, 'T15031559', 'Chain', 22, 8.00, 8.00, 27779.00, 25001.00),
(68, 'T15031560', 'Earrings', 24, 8.00, 8.00, 30555.00, 27500.00),
(69, 'T15031561', 'Bracelet', 24, 8.00, 8.00, 30555.00, 27500.00),
(70, 'T15031562', 'Chain', 22, 8.00, 8.00, 27779.00, 25001.00),
(71, 'T15031563', 'Bracelet', 24, 8.00, 8.00, 30555.00, 27500.00),
(72, 'T15031564', 'Chain', 24, 8.00, 8.00, 30555.00, 27500.00),
(73, 'T15031565', 'Earrings', 24, 8.00, 8.00, 30555.00, 27500.00),
(74, 'T15031566', 'Bracelet', 24, 8.00, 8.00, 30555.00, 27500.00),
(75, 'T15031567', 'Earrings', 24, 8.00, 8.00, 30555.00, 27500.00),
(76, 'T15031568', 'Bangle', 24, 8.00, 8.00, 30555.00, 27500.00),
(77, 'T15031569', 'Bangle', 24, 8.00, 8.00, 30555.00, 27500.00),
(78, 'T15031570', 'Bangle', 24, 8.00, 8.00, 30555.00, 27500.00),
(79, 'T15031571', 'Chain', 22, 8.00, 8.00, 27779.00, 25001.00),
(80, 'T15031572', 'Bracelet', 22, 8.00, 8.00, 27779.00, 25001.00),
(81, 'T15031673', 'Bangle', 24, 8.00, 8.00, 30555.00, 27500.00);

-- --------------------------------------------------------

--
-- Table structure for table `ticket_status`
--

CREATE TABLE IF NOT EXISTS `ticket_status` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `ticket_no` varchar(15) NOT NULL,
  `capital_remain` float(8,2) NOT NULL,
  `interest_remain` float(8,2) NOT NULL,
  `last_payment_update` datetime NOT NULL,
  `status` int(1) NOT NULL,
  `repawned` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ticket_no` (`ticket_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `ticket_status`
--

INSERT INTO `ticket_status` (`id`, `ticket_no`, `capital_remain`, `interest_remain`, `last_payment_update`, `status`, `repawned`) VALUES
(1, 'T000009', 0.00, 0.00, '2014-06-24 20:00:56', 0, 1),
(2, 'T000001', 0.00, 0.00, '2013-11-14 21:55:38', 0, 1),
(3, 'T000002', 0.00, 0.00, '2014-01-07 00:12:43', 0, 0),
(8, 'T000010', 50000.00, 5707.52, '2015-03-15 11:11:17', 1, 0),
(10, 'T000015', 30000.00, 3431.40, '2015-03-16 09:33:28', 1, 0),
(13, 'T000019', 0.00, 0.00, '2015-02-16 21:52:29', 0, 0),
(17, 'T000024', 32682.77, 10938.56, '2015-03-14 11:55:52', 1, 0),
(18, 'T000027', 84260.84, 0.00, '2014-08-28 17:59:25', 1, 0),
(19, 'T000028', 22093.12, 32.32, '2015-03-15 10:09:39', 1, 0),
(20, 'T000029', 12964.12, 933.16, '2015-03-15 10:03:57', 1, 0),
(21, 'T000030', 100000.00, 19.28, '2015-02-28 13:19:42', 1, 0),
(23, 'T000031', 20000.00, 0.00, '2015-02-08 21:59:17', 1, 0),
(24, 'T000033', 25000.00, 0.00, '2015-02-11 22:17:11', 1, 0),
(25, 'T000034', 4000.00, 0.00, '2015-02-14 13:04:59', 1, 0),
(26, 'T15021835', 40000.00, 0.00, '2015-02-18 15:21:22', 1, 0),
(27, 'T15022336', 35000.00, 0.00, '2015-02-23 17:13:57', 1, 0),
(28, 'T15022537', 15000.00, 0.00, '2015-02-25 18:44:05', 1, 0),
(29, 'T15022538', 2055.23, 0.00, '2015-02-27 20:30:37', 1, 0),
(30, 'T15022539', 1500.00, 0.00, '2015-02-25 20:37:41', 1, 0),
(31, 'T15022540', 25000.00, 0.00, '2015-02-25 20:39:24', 1, 0),
(32, 'T15022541', 10000.00, 0.00, '2015-02-25 21:06:59', 1, 0),
(33, 'T15022542', 1222.00, 0.00, '2015-02-25 21:20:39', 1, 0),
(34, 'T15022543', 222.00, 0.00, '2015-02-25 21:45:29', 1, 0),
(35, 'T15022644', 2013.81, 0.00, '2015-03-01 23:15:41', 1, 0),
(36, 'T15022645', 0.00, 0.00, '2015-02-27 11:19:27', 0, 0),
(37, 'T15022746', 20000.00, 0.00, '2015-02-27 11:15:43', 3, 0),
(38, 'T15022747', 0.00, 0.00, '2015-03-15 10:30:08', 0, 1),
(39, 'T15022748', 0.00, 0.00, '2015-02-27 13:15:05', 0, 0),
(40, 'T15031449', 0.00, 0.00, '2015-03-14 12:52:26', 0, 1),
(41, 'T15031450', 0.00, 0.00, '2015-03-14 11:37:19', 0, 1),
(42, 'T15031451', 0.00, 0.00, '2015-03-14 12:46:22', 0, 0),
(43, 'T15031452', 0.00, 0.00, '2015-03-15 20:19:41', 0, 1),
(44, 'T15031453', 25000.00, 0.00, '2015-03-14 12:52:26', 1, 0),
(45, 'T15031454', 1000.00, 0.00, '2015-03-14 13:37:28', 3, 0),
(46, 'T15031455', 0.00, 0.00, '2015-03-15 10:16:09', 0, 1),
(47, 'T15031456', 0.00, 0.00, '2015-03-15 09:50:00', 0, 1),
(48, 'T15031457', 1500.00, 0.00, '2015-03-14 21:12:36', 3, 0),
(49, 'T15031558', 0.00, 0.00, '2015-03-15 09:51:02', 0, 1),
(50, 'T15031559', 0.00, 0.00, '2015-03-15 10:33:57', 0, 1),
(51, 'T15031560', 0.00, 0.00, '2015-03-15 16:40:10', 0, 1),
(52, 'T15031561', 0.00, 0.00, '2015-03-15 11:05:32', 0, 1),
(53, 'T15031562', 23800.00, 0.00, '2015-03-15 10:33:57', 1, 0),
(54, 'T15031563', 0.00, 0.00, '2015-03-15 16:16:15', 0, 1),
(55, 'T15031564', 1200.00, 0.00, '2015-03-15 16:05:17', 3, 0),
(56, 'T15031565', 15000.00, 0.00, '2015-03-15 16:06:02', 1, 0),
(57, 'T15031566', 22000.00, 0.00, '2015-03-15 16:16:15', 1, 0),
(58, 'T15031567', 6504.00, 0.00, '2015-03-15 16:40:10', 1, 0),
(59, 'T15031568', 1500.00, 0.00, '2015-03-15 20:01:57', 3, 0),
(60, 'T15031569', 2000.00, 0.00, '2015-03-15 20:02:37', 3, 0),
(61, 'T15031570', 2500.00, 0.00, '2015-03-15 20:19:41', 1, 0),
(62, 'T15031571', 12000.00, 0.00, '2015-03-15 20:32:34', 3, 0),
(63, 'T15031572', 10000.00, 0.00, '2015-03-15 20:33:17', 1, 0),
(64, 'T15031673', 27500.00, 0.00, '2015-03-16 09:29:18', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(128) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(75) NOT NULL,
  `user_email` varchar(128) NOT NULL,
  `user_type` int(2) NOT NULL,
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `user_type` (`user_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `full_name`, `username`, `password`, `user_email`, `user_type`, `last_login`) VALUES
(1, 'Farzath Faiz', 'farzath', '9c8cafccffe06d16454788b80aee4ea3', 'farzath@mooiwebs.com', 1, '2015-03-19 10:56:35'),
(4, 'Nalaka', 'nalaka', '202cb962ac59075b964b07152d234b70', 'nalakanayana@gmail.com', 1, '2015-03-16 09:47:42'),
(5, 'User test', 'test123', '202cb962ac59075b964b07152d234b70', 'mail2farzath@yahoo.com', 2, '2015-03-14 13:31:36'),
(6, 'nalaka', 'nalaka1', '202cb962ac59075b964b07152d234b70', 'nalakanayana@gmail.com', 2, '2015-03-15 07:42:24'),
(7, 'sankalpana karunarathna', 'sankalpana', 'b58c7781daa0d5f2093b85011b7f0fb1', 'san102030@yahoo.com', 2, '2015-08-24 21:33:45');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auctions`
--
ALTER TABLE `auctions`
  ADD CONSTRAINT `auctions_ibfk_1` FOREIGN KEY (`ticket_no`) REFERENCES `ticket_articles` (`ticket_no`) ON UPDATE CASCADE;

--
-- Constraints for table `inquiries`
--
ALTER TABLE `inquiries`
  ADD CONSTRAINT `inquiries_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`nic`) ON UPDATE CASCADE;

--
-- Constraints for table `payment_history`
--
ALTER TABLE `payment_history`
  ADD CONSTRAINT `payment_history_ibfk_1` FOREIGN KEY (`ticket_no`) REFERENCES `tickets` (`ticket_no`) ON UPDATE CASCADE;

--
-- Constraints for table `repawn_history`
--
ALTER TABLE `repawn_history`
  ADD CONSTRAINT `repawn_history_ibfk_1` FOREIGN KEY (`old_ticket_no`) REFERENCES `tickets` (`ticket_no`),
  ADD CONSTRAINT `repawn_history_ibfk_2` FOREIGN KEY (`new_ticket_no`) REFERENCES `tickets` (`ticket_no`),
  ADD CONSTRAINT `repawn_history_ibfk_3` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`nic`) ON UPDATE CASCADE;

--
-- Constraints for table `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `tickets_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`nic`) ON UPDATE CASCADE;

--
-- Constraints for table `ticket_articles`
--
ALTER TABLE `ticket_articles`
  ADD CONSTRAINT `ticket_articles_ibfk_1` FOREIGN KEY (`ticket_no`) REFERENCES `tickets` (`ticket_no`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ticket_status`
--
ALTER TABLE `ticket_status`
  ADD CONSTRAINT `ticket_status_ibfk_1` FOREIGN KEY (`ticket_no`) REFERENCES `tickets` (`ticket_no`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
