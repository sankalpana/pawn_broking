<div class="row">
    <h3 class="page-title">New pawning</h3>

    <form role="form" class="form-horizontal" id="valuate_pawning" name="frm" method="POST"
          action="<?php current_url(); ?>">
        <div class="col-md-10 col-md-offset-1">
            <div class="row data-area">
                <fieldset>
                    <legend>Evaluate pawning</legend>
                    <div class="row">
                        <div class="col-md-3"><label class="control-label" for="article">Article</label></div>
                        <div class="col-md-3"><label class="control-label" for="karat">karatage</label></div>
                        <div class="col-md-2"><label class="control-label" for="gross_weight">Gross weight (g)</label>
                        </div>
                        <div class="col-md-2"><label class="control-label" for="net_weight">Net weight (g)</label></div>
                        <div class="col-md-2"></div>
                    </div>
                    <div class="row">
                        <!-- Select article-->
                        <div class="col-md-3">
                            <select id="article" name="article" class="form-control">
                                <option value="">Select article</option>
                                <?php foreach($articles as $row): ?>
                                    <option value="<?php echo $row['article_name']; ?>"><?php echo $row['article_name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <!-- Select karatage-->
                        <div class="col-md-3">

                            <select id="karat" name="karat" class="form-control">
                                <option value="">Select karat</option>
                                <?php foreach ($karatage as $row): ?>
                                    <option
                                        value="<?php echo $row['karat_value'] . '-' . $row['assessed_value'] . '-' . $row['payable_value']; ?>">
                                        <?php echo $row['karat_value']; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <!-- Gross weight-->
                        <div class="col-md-2">
                            <input id="gross_weight" name="gross_weight" type="text" class="form-control input-md">
                        </div>

                        <!-- Net weight-->
                        <div class="col-md-2">
                            <input id="net_weight" name="net_weight" type="text" class="form-control input-md">
                        </div>
                        <!-- add button -->
                        <div class="col-md-2">
                            <button name="add_article" id="add_article" class="btn btn-default btn-block" type="button"
                                    onclick="addRow(this.form);">Add
                            </button>
                        </div>
                    </div>
                </fieldset>
                <div class="row" id="added_articles_desc" style="margin-top:50px;">
                    <div class="col-md-2"><label class="control-label" for="article">Article</label></div>
                    <div class="col-md-2"><label class="control-label" for="karat">karat</label></div>
                    <div class="col-md-1"><label class="control-label" for="gross_weight">Gross(g)</label></div>
                    <div class="col-md-1"><label class="control-label" for="net_weight">Net(g)</label></div>
                    <div class="col-md-2"><label class="control-label" for="assessed_value">Assessed value Rs.</label>
                    </div>
                    <div class="col-md-2"><label class="control-label" for="payable_value">Payable value Rs.</label>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row" id="added_articles">
                </div>
                <div class="row" id="advance_pay" style="margin-top:25px;">
                    <div class="col-md-6">
                        <label class="col-md-4 control-label" for="total_payable">Total payable amount</label>

                        <div class="col-md-8">
                            <input id="total_payable" name="total_payable" type="text" class="form-control input-md"
                                   value="0" readonly required="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="col-md-4 control-label" for="advance_amount">Advanced Amount</label>

                        <div class="col-md-8">
                            <input id="advanced_amount" name="advanced_amount" type="text" class="form-control input-md"
                                   value="" required="">
                            <small>Min. Amount: <?php echo $this->session->userdata('min_advance_amount');?></small>
                        </div>
                    </div>


                    <div class="col-md-4 col-md-offset-8">
                        <input name="cc" id="cc" class="cc" type="radio" value="1" disabled="true"/>
                        <label for="cc">Confirm & register customer</label>
                    </div>

                </div>

            </div>
            <div class="row data-area" id="customer_register">
                <fieldset>
                    <legend>Customer details</legend>
                    <div class="col-md-6">

                        <!-- Customer NIC-->
                        <div class="form-group">
                            <label class="col-md-5 control-label" for="customer-nic">Customer NIC</label>

                            <div class="col-md-7">
                                <input id="customer-nic" name="customer-nic" type="text" class="form-control input-md"
                                       value="" required="" pattern="[0-9]{9}[v-x]" maxlength="10"
                                       title="NIC number must be with 9 number and simple v/x" onBlur="getagentids();">

                            </div>
                        </div>

                        <!-- Customer name-->
                        <div class="form-group">
                            <label class="col-md-5 control-label" for="customer-name">Customer name</label>

                            <div class="col-md-7">
                                <input id="customer-name" name="customer-name" type="text" class="form-control input-md"
                                       value="" pattern=".{5,}" required title="5 characters minimum">

                            </div>
                        </div>

                        <!-- Email -->
                        <div class="form-group">
                            <label class="col-md-5 control-label" for="email">Email</label>

                            <div class="col-md-7">
                                <input id="email" name="email" type="email" class="form-control input-md" value="">

                            </div>
                        </div>

                        <!-- Contact no -->
                        <div class="form-group">
                            <label class="col-md-5 control-label" for="contact_no">Contact no.</label>

                            <div class="col-md-7">
                                <input id="contact_no" name="contact_no" type="text" class="form-control input-md"
                                       value="" required="" pattern="[0-9]{10,10}" maxlength="10"
                                       title="Provide valide contact number">

                            </div>
                        </div>

                        <!-- Mobile no -->
                        <div class="form-group">
                            <label class="col-md-5 control-label" for="mobile_no">Mobile no.</label>

                            <div class="col-md-7">
                                <input id="mobile_no" name="mobile_no" type="text" class="form-control input-md"
                                       value="" pattern="[0-9]{10,10}" maxlength="10">

                            </div>
                        </div>

                        <!-- Submit Button -->
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-7">
                                <button id="issue-pawn" name="issue-pawn" class="btn btn-primary btn-block"
                                        type="submit">Submit & Pawn Articles
                                </button>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-6">

                        <!-- Address 1-->
                        <div class="form-group">
                            <label class="col-md-5 control-label" for="address1">Address line 1</label>

                            <div class="col-md-7">
                                <input id="address1" name="address1" type="text" class="form-control input-md" value=""
                                       required="">

                            </div>
                        </div>

                        <!-- Address 2-->
                        <div class="form-group">
                            <label class="col-md-5 control-label" for="address2">Address line 2</label>

                            <div class="col-md-7">
                                <input id="address2" name="address2" type="text" class="form-control input-md" value=""
                                       required="">

                            </div>
                        </div>

                        <!-- Address 2-->
                        <div class="form-group">
                            <label class="col-md-5 control-label" for="city">City</label>

                            <div class="col-md-7">
                                <input id="city" name="city" type="text" class="form-control input-md" value=""
                                       required="">

                            </div>
                        </div>

                        <!-- Postal code-->
                        <div class="form-group">
                            <label class="col-md-5 control-label" for="postal-code">Postal code</label>

                            <div class="col-md-7">
                                <input id="postal-code" name="postal-code" type="text" class="form-control input-md"
                                       value="">
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">

    $('#added_articles_desc').hide();
    $('#advance_pay').hide();
    $('#customer_register').hide();

    var rowNum = 0;

    function addRow(frm) {
        var gross_weight = $('#gross_weight').val();
        var net_weight = $('#net_weight').val();

        if ($('#article').val() == '') {
            alert("Please select pawn article");
        } else if ($('#gross_weight').val() == '') {
            alert("Gross weight can not be empty");
        } else if ($('#net_weight').val() == '') {
            alert("Net weight can not be empty");
        } else if (+net_weight > +gross_weight) {
            alert("Net weight can not be over than gross weight");
        } else if (+net_weight < <?php echo $this->session->userdata('min_net_weight');?>) {
            alert("Gross and Net weight can not lower than <?php echo $this->session->userdata('min_net_weight');?>");
        } else {
            $('#added_articles_desc').show();
            $('#advance_pay').show();

            rowNum++;
            var vals = $('#karat').val().split('-');
            var totPayable = parseInt(frm.total_payable.value);
            var assessed_val = (parseInt(vals[1]) / 8) * (net_weight);
            var payable_val = (parseInt(vals[2]) / 8) * (net_weight);
            var row = '<div style="margin-top:10px;" id="rowNum' + rowNum + '"><div class="col-md-2"> <input name="article[]" type="text" class="form-control input-md" value="' + article.value + '" readonly></div><div class="col-md-2"> <input name="karat[]" type="text" class="form-control input-md" value="' + vals[0] + '" readonly></div><div class="col-md-1"> <input name="gross_weight[]" type="text" class="form-control input-md" value="' + gross_weight + '" readonly></div><div class="col-md-1"> <input name="net_weight[]" type="text" class="form-control input-md" value="' + net_weight + '" readonly></div><div class="col-md-2"> <input name="assessed_value[]" type="text" class="form-control input-md" value="' + assessed_val + '" readonly></div><div class="col-md-2"> <input id="payable_value' + rowNum + '" name="payable_value[]" type="text" class="form-control input-md" value="' + payable_val + '" readonly></div><div class="col-md-2"><button name="remove_article" id="remove_article" class="btn btn-default btn-block" type="button" onclick="removeRow(' + rowNum + ');">Remove</button></div><div class="clearfix"></div></div>';
            jQuery('#added_articles').append(row);

            $('#article').val('');
            frm.gross_weight.value = '';
            frm.net_weight.value = '';

            totPayable += payable_val;
            frm.total_payable.value = totPayable;
        }
    }

    function removeRow(rnum) {
        del_totPayable = frm.total_payable.value;

        del_pay_val = $('#payable_value' + rnum).val();
        del_totPayable -= del_pay_val;
        frm.total_payable.value = del_totPayable;

        jQuery('#rowNum' + rnum).remove();

        if (del_totPayable == 0) {
            $('#added_articles_desc').hide();
            $('#advance_pay').hide();
        }
    }

    $('#advanced_amount').on('keyup keypress blur change', function() {

        var adv_amount = $("#advanced_amount").val();
        var tot_payable = $("#total_payable").val();

        if ((parseInt(adv_amount) >= <?php echo $this->session->userdata('min_advance_amount');?>)&&(parseInt(adv_amount) <= parseInt(tot_payable))){
            $("#cc").prop("disabled", false);
            //$('#customer_register').fadeIn();
        }else{
            $("#cc").prop("disabled", true);
            $('#customer_register').fadeOut();
            $('#cc').prop('checked', false);
        }
    });

    $("#cc").click(function(){
        $('#customer_register').fadeIn();
    });

    $(document).ready(function() {
        $('#progress').hide();

        $('#valuate_pawning').ajaxForm({

            target: '#issued_ticket',
            beforeSubmit: function() {
                $('#progress').show();
            },
            resetForm: false,
            success: function() {
                $('#valuate_pawning').fadeOut();
                $('#customer_register').fadeOut();
                $('#progress').fadeOut('slow');
                $('#issued_ticket').fadeIn('slow');
            }
        });
    });
</script>