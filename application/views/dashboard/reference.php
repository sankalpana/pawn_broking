<div class="col-md-12">
    <h3 class="highlight">Today's Interest Rate is <?php echo $this->session->userdata('interest_rate');?></h3>
</div>

<div class="col-md-8 col-md-offset-4">
    <fieldset>
        <legend>Values for karatage</legend>
        <table class="table">
            <thead>
            <tr>
                <th>Karat</th>
                <th>Assessed Values</th>
                <th>Payable Value</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($karatage as $row): ?>
                    <tr>
                        <td><?php echo $row['karat_value'];?>k</td>
                        <td><?php echo $row['assessed_value'];?></td>
                        <td><?php echo $row['payable_value'];?></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </fieldset>
</div>
