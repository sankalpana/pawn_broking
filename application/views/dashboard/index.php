<div class="container">
    <h3 class="page-title">Dashboard</h3>

    <div class="row">
        <div class="container">
            <div class="col-md-2 data-area text-center col-md-offset-8">
                <a href="<?php echo site_url('pawn/create'); ?>"><h4>New Pawn</h4></a>
                <a href="?page=pawn&type=list"><strong>Manage Pawn Articles</strong></a>
            </div>

            <div class="col-md-2 data-area text-center">
                <a href="?page=customer&&type=list"><h4>Customers</h4></a>
                <a href="?page=customer&&type=list"><strong>Manage</strong></a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="container">
        </div>
    </div>
</div>
