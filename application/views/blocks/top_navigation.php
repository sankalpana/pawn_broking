<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#orchid-pawn-navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo current_url(); ?>">Dashboard</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="orchid-pawn-navigation">

            <!--<form class="navbar-form navbar-left" action=""  method="GET" role="form">-->
            <div class="form-group navbar-form navbar-left">
                <input type="text" name="ticket-no" class="form-control search" id="searchid" placeholder="Ticket No / NIC No." required>
                <div id="result"></div>
            </div>
            <!--</form>-->

            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Pawning <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo site_url('pawn/create'); ?>">New Pawn</a></li>
                        <li><a href="?page=pawn&type=list">Pawned</a></li>
                        <?php if($this->session->userdata('user_type') == 1): ?>
                            <li role="presentation" class="divider"></li>
                            <li><a href="?page=customer&&type=list">Customers</a></li>
                            <li><a href="?page=customer&&type=inquiries">Inquiries</a></li>
                            <li><a href="?page=customer&&type=alert">Email Alert</a></li>
                        <?php endif;?>
                        <!--<li><a href="?page=payment">Payment</a></li>-->
                    </ul>
                </li>
                <li><a class="fancybox" href="<?php echo site_url('dashboard/reference'); ?>">References</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php if($this->session->userdata('user_type') == 1): ?>
                    <!-- Auctions -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Auction<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="?page=auction&type=mark">Mark for Auction</a></li>
                            <li><a href="?page=auction&type=unmark">Unmark from Auction</a></li>
                        </ul>
                    </li>

                    <!-- Reports -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Reports<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="?page=report&type=daily">Daily</a></li>
                            <li><a href="?page=report&type=thismonth">This Month</a></li>
                            <li><a href="?page=report&type=all">All</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">User Management<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="?page=user&type=list">All users</a></li>
                            <li><a href="?page=user&type=new">Add new user</a></li>
                        </ul>
                    </li>
                <?php endif;?>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Logged As <?php echo (($this->session->userdata('user_type') == 1) ? 'Manager ' : 'Officer ');?> <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="?page=user&type=edit&id=<?php echo $this->session->userdata('user_id');?>">Edit profile</a></li>
                        <?php if($this->session->userdata('user_type') == 1): ?>
                            <li><a href="?page=options">Settings</a></li>
                        <?php endif;?>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url('User/logout'); ?>">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>