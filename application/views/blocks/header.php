<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>New Orchid Jewellers - Pawning system</title>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link type="image/png" rel="icon"  href="<?php echo base_url("assets/img/favicon.png"); ?>" />

    <!--css files-->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/normalize.css"); ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.css"); ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/custom.css"); ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/css/datepicker.css"); ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url("assets/fancybox/jquery.fancybox.css"); ?>">


    <!--js files-->
    <script type="text/javascript" src="<?php echo base_url("assets/js/jquery-1.10.2.min.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.min.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/jquery.form.min.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap-datepicker.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/modernizr-2.6.2.min.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/live-time.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/search.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/plugins.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/mypagination.js"); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/fancybox/jquery.fancybox.js"); ?>"></script>
    <script type="text/javascript">
        $(function(){
            $(".fancybox").fancybox({
                type        : 'iframe',
                autoSize : true,
                width    : "80%",
                height   : "60%",
                'scrolling'     : 'no',
                'titleShow'     : false,
                openEffect  : 'fade',
                closeEffect : 'fade'
            });
        });
    </script>

</head>
<body>