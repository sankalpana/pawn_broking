<div class="row header">
    <div class="container">
        <div class="col-md-4"><img src="<?php echo base_url('assets/img/NDJ-Logo.png'); ?>"/></div>
        <div class="col-md-4"><br/><h2 class="text-center"><strong>Pawn Broking System</strong></h2></div>
        <div class="col-md-4 top-bar text-right">
            <!-- Javascript real date and time display-->
            <span id="date_time"></span>
            <script type="text/javascript">window.onload = date_time('date_time');</script>
            <div><strong>Welcome, <?php echo $this->session->userdata('username'); ?></strong></div>
        </div>
    </div>
</div>