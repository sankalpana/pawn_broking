<div class="row">
    <div class="container">
        <div class="col-md-4 col-md-offset-4">

            <form class="form-signin" role="form" action="<?php current_url(); ?>" id="login" name="login" method="POST">
                <div class="text-center"><img src="<?php echo base_url('assets/img/NDJ-Logo.png'); ?>"/></div>
                <div id="msgbox"></div>
                <h2 class="form-signin-heading">Please sign in</h2>

                <!-- Text input-->
                <div class="form-group">
                    <label class="control-label" for="username">Username</label>

                    <div>
                        <input id="username" name="username" placeholder="Username" class="form-control input-md" type="text" required>
                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group">
                    <label class="control-label" for="password">Password</label>

                    <div>
                        <input id="password" name="password" placeholder="Password" class="form-control input-md"
                               type="password">
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <div>
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                    </div>
                </div>
            </form>

            <div class="text-right">
                <small><a id="forgot_pass" href="#">Forgot password?</a></small>
            </div>
            <form class="form-signin" role="form" action="forgot_pass.php" id="frm_forgot_pass" name="frm_forgot_pass"
                  method="POST">
                <div class="text-center"><img src="<?php echo base_url('assets/img/NDJ-Logo.png'); ?>"/></div>
                <div id="msgbox1"></div>
                <h2 class="form-signin-heading">Reset password</h2>
                <!-- email input-->
                <div class="form-group">
                    <label class="control-label" for="email">Enter your email</label>

                    <div>
                        <input id="email" name="email" placeholder="Email" class="form-control input-md" required=""
                               type="email" title="Provide your registered email">
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <div>
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#frm_forgot_pass').hide();

    $('#forgot_pass').on('click', function () {
        $('#forgot_pass').hide();
        $('#login').fadeOut('slow');
        $('#frm_forgot_pass').fadeIn('slow');

    });

    $(document).ready(function () {
        $("#login").submit(function () {
            //remove all the class add the messagebox classes and start fading
            $("#msgbox").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
        });

        $("#frm_forgot_pass").submit(function () {
            //remove all the class add the messagebox classes and start fading
            $("#msgbox1").removeClass().addClass('messagebox').text('Validating....').fadeIn(1000);
            //check the username exists or not from ajax
            $.post("forgot_pass.php", {email: $('#email').val(), rand: Math.random()}, function (data) {
                if (data) //if correct login detail
                {
                    $("#msgbox1").fadeTo(200, 0.1, function ()  //start fading the messagebox
                    {
                        //add message and change the class of the box and start fading
                        $(this).html(data).addClass('messageboxok').fadeTo(900, 1);

                    });
                }
                else {
                    $("#msgbox1").fadeTo(200, 0.1, function () //start fading the messagebox
                    {
                        //add message and change the class of the box and start fading
                        $(this).html('Invalid login details...').addClass('messageboxerror').fadeTo(900, 1);
                    });
                }

            });
            return false; //not to post the  form physically
        });


        //now call the ajax also focus move from
        /*$("#password").blur(function()
         {
         $("#login").trigger('submit');
         });
         */
    });
</script>