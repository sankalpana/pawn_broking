<?php

class User_model extends CI_Model {

    public $validations = array(
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'required|min_length[3]|max_length[50]'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required|min_length[3]|max_length[500]'
        )
    );

    function validate_user($data){
        $this->db->from('user');
        $this->db->where('username',$data['username']);
        $this->db->where('password',md5($data['password']));
        $query = $this->db->get();

        //return array of arrays if data available
        if ($query->num_rows() > 0)
        {
            $row = $query->row_array();
            return $row;
        }
        // return false if no data
        else
        {
            return FALSE;
        }
    }

    function get_settings(){
        //query
        $query = $this->db->query('SELECT * FROM options');

        //return array if data available
        if ($query->num_rows() > 0)
        {
            $row = $query->row_array();
            return $row;
        }
        // return false if no data
        else
        {
            return FALSE;
        }
    }


}