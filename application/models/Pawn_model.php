<?php

class Pawn_model extends CI_Model {

    function return_result_array($query){
        //return array of arrays if data available
        if ($query->num_rows() > 0)
        {
            $row = $query->result_array();
            return $row;
        }
        // return false if no data
        else
        {
            return FALSE;
        }
    }

    function get_karatage(){
        //query
        $query = $this->db->query('SELECT * FROM karatage ORDER BY karat_value DESC');

        //return the array return by function
        return $this->return_result_array($query);
    }

    function get_articles(){
        //query
        $query = $this->db->query('SELECT * FROM articles ORDER BY article_name');

        //return the array return by function
        return $this->return_result_array($query);
    }


}