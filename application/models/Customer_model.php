<?php
class Customer_model extends CI_Model{

    public $validations = array(
        array(
            'field' => 'customer-nic',
            'label' => 'NIC',
            'rules' => 'required|min_length[3]|max_length[10]'
        ),
        array(
            'field' => 'customer-name',
            'label' => 'Name',
            'rules' => 'required|min_length[3]|max_length[60]'
        ),
        array(
            'field' => 'address1',
            'label' => 'Address 1',
            'rules' => 'required|min_length[3]|max_length[100]'
        )
    );

    function get_result_array($query){
        if ($query->num_rows() == 0)
        {
            //return false if no data
            return FALSE;
        }
        elseif($query->num_rows() == 1)
        {
            //return array if one row
            $row = $query->row_array();
            return $row;
        }
        else
        {
            // return array of arrays if multiple rows
            $row = $query->result_array();
            return $row;
        }
    }

    function get_customer_by_nic($nic){
        //$query = $this->db->query('SELECT customer.id FROM customers where customers.nic = '.$nic);
        $this->db->select('id')->where('nic', $nic)->from('customers');
        $query = $this->db->get();// produce : select id from customers where nic = ##
        //return the array return by function
        return $this->get_result_array($query);
    }

    function create_customer($data){
        $this->db->insert('customers', $data);
    }

    function update_customer($id,$data){
        $this->db->where('id', $id);
        $this->db->update('customers', $data);
    }


}