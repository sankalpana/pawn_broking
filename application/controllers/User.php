<?php

class User extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
    }

    function render_login(){
        $this->load->view('blocks/header');
        $this->load->view('user/login');
        $this->load->view('blocks/footer');
    }

    //  this method set as default in config/routes
    function login(){

        // set validation rules
        $this->form_validation->set_rules($this->User_model->validations);

        //validate inputs
        if ($this->form_validation->run() === TRUE) {

            //validate user
            $validated_user = $this->User_model->validate_user(
                array(
                    'username' => $this->input->post('username'),
                    'password' => $this->input->post('password')
                )
            );

            if($validated_user){
                // Set user details to a session
                $this->session->set_userdata('user_id', $validated_user['id']);
                $this->session->set_userdata('username', $validated_user['username']);
                $this->session->set_userdata('user_type', $validated_user['user_type']);
                $this->session->set_userdata('is_logged', true);

                //get settings
                $settings = $this->User_model->get_settings();

                // Set settings
                $this->session->set_userdata('time_zone', $settings['time_zone']);
                $this->session->set_userdata('interest_rate', $settings['interest_rate']);
                $this->session->set_userdata('session_time_out', $settings['session_time_out']);
                $this->session->set_userdata('min_net_weight', $settings['min_net_weight']);
                $this->session->set_userdata('min_advance_amount', $settings['min_advance_amount']);
                $this->session->set_userdata('min_payment', $settings['min_payment']);

                //redirect
                redirect(site_url("dashboard/index"), 'refresh');

            }else{
                $this->session->set_userdata('is_logged', false);
                $this->render_login();
            }
        }else{
            $this->render_login();
        }
    }

    function logout(){
        $this->session->sess_destroy();
        redirect(site_url('User/login'),'refresh');
    }
}