<?php

class Dashboard extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pawn_model');
    }

    function render_login(){
        $this->load->view('blocks/header');
        $this->load->view('user/login');
        $this->load->view('blocks/footer');
    }

    function render($view_file,$data = array()){
        $this->load->view('blocks/header');
        $this->load->view('blocks/application_header');
        $this->load->view($view_file,$data);
        $this->load->view('blocks/application_footer');
        $this->load->view('blocks/footer');
    }

    function render_popup($view_file,$data = array()){
        $this->load->view('blocks/header');
        $this->load->view($view_file,$data);
        $this->load->view('blocks/application_footer');
        $this->load->view('blocks/footer');
    }

    //  this method set as default in config/routes
    function index(){
        if($this->session->userdata('is_logged')){
            $this->render('dashboard/index');
        }else{
            $this->render_login();
        }
    }

    function reference(){
        if($this->session->userdata('is_logged')){
            $data['karatage']=$this->Pawn_model->get_karatage();
            $this->render_popup('dashboard/reference',$data);
        }else{
            $this->render_login();
        }
    }

}