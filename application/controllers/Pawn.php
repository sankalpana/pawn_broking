<?php

class Pawn extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pawn_model');
        $this->load->model('Customer_model');
    }

    function render($view_file, $data = array())
    {
        $this->load->view('blocks/header');
        $this->load->view('blocks/application_header');
        $this->load->view($view_file, $data);
        $this->load->view('blocks/application_footer');
        $this->load->view('blocks/footer');
    }

    function render_login()
    {
        $this->load->view('blocks/header');
        $this->load->view('user/login');
        $this->load->view('blocks/footer');
    }

    function create()
    {
        //check wether user is logged , shorthand if else
        $this->session->userdata('is_logged') ? true : $this->render_login();

        //set validation rules
        $this->form_validation->set_rules($this->Customer_model->validations);

        //validate inputs
        if ($this->form_validation->run() === TRUE) {
            // convert nic to upper case
            $nic = strtoupper($_POST['customer-nic']);

            //check customer exists before insert
            $result = $this->Customer_model->get_customer_by_nic($nic);

            if($result){
                //update if customer exist
                $this->Customer_model->update_customer($result['id'],
                    array(
                        'nic' => $nic,
                        'name' => $_POST['customer-name'],
                        'address1' => $_POST['address1'],
                        'address2' => $_POST['address2'],
                        'city' => $_POST['city'],
                        'postal_code' => $_POST['postal-code'],
                        'email' => $_POST['email'],
                        'contact_no' => $_POST['contact_no'],
                        'mobile_no' => $_POST['mobile_no'],
                    ));

            }else{
                //create if customer not exist
                $this->Customer_model->create_customer(array(
                    'nic' => $nic,
                    'name' => $_POST['customer-name'],
                    'address1' => $_POST['address1'],
                    'address2' => $_POST['address2'],
                    'city' => $_POST['city'],
                    'postal_code' => $_POST['postal-code'],
                    'email' => $_POST['email'],
                    'contact_no' => $_POST['contact_no'],
                    'mobile_no' => $_POST['mobile_no'],
                ));
            }

            redirect(site_url("dashboard/index"), 'refresh');
        }else{
            //data
            $data['articles'] = $this->Pawn_model->get_articles();
            $data['karatage'] = $this->Pawn_model->get_karatage();

            //calling render function
            $this->render('pawn/create', $data);
        }
    }

    function reference()
    {
        if ($this->session->userdata('is_logged')) {
            $data['karatage'] = $this->Pawn_model->get_karatage();
            $this->render('dashboard/reference', $data);
        } else {
            $this->render_login();
        }
    }
}